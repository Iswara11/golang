package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	var panjangPersegiPanjang string = "8"
	store1, _ := strconv.ParseInt(panjangPersegiPanjang, 10, 64)
	var lebarPersegiPanjang string = "5"
	store2, _ := strconv.ParseInt(lebarPersegiPanjang, 10, 64)
	var alasSegitiga string = "6"
	store3, _ := strconv.ParseFloat(strings.TrimSpace(alasSegitiga), 64)
	var tinggiSegitiga string = "7"
	store4, _ := strconv.ParseFloat(strings.TrimSpace(tinggiSegitiga), 64)
	//var number_decimal float64 = 0.5

	luasPersegiPanjang := store1 * store2
	fmt.Println(luasPersegiPanjang)
	kelilingPersegiPanjang := 2 * (store1 + store2)
	fmt.Println(kelilingPersegiPanjang)
	luasSegitiga := 0.5 * store3 * store4
	fmt.Println(luasSegitiga)

	//nilaiDoe := 50
	nilaiJohn := 70

	//if (nilaiJohn >= 80) && (nilaiDoe >= 80) {
	//	fmt.Println("A")
	//} else if (nilaiJohn >= 70) && (nilaiJohn < 80) {
	//	fmt.Println("belanjaan saya tidak lengkap")
	//} else if (nilaiDoe >= 80) && (nilaiJohn < 80) {
	//	fmt.Println("belanjaan saya tidak lengkap")
	//} else if telur == "soldout" {
	//	fmt.Println("telur habis")
	//} else if buah == "soldout" {
	//	fmt.Println("buah habis")
	//}

	if nilaiJohn >= 80 {
		fmt.Println("A")
	} else if (nilaiJohn >= 70) && (nilaiJohn < 80) {
		fmt.Println("B")
	} else if (nilaiJohn >= 60) && (nilaiJohn < 70) {
		fmt.Println("C")
	} else if (nilaiJohn >= 50) && (nilaiJohn < 60) {
		fmt.Println("D")
	} else {
		fmt.Println("D")
	}

	nilaiDoe := 50

	if nilaiDoe >= 80 {
		fmt.Println("A")
	} else if (nilaiDoe >= 70) && (nilaiDoe < 80) {
		fmt.Println("B")
	} else if (nilaiDoe >= 60) && (nilaiDoe < 70) {
		fmt.Println("C")
	} else if (nilaiDoe >= 50) && (nilaiDoe < 60) {
		fmt.Println("D")
	} else {
		fmt.Println("D")
	}
}
