package main

import (
	"fmt"
	"strconv"
)

// tugas ke 2 soal no 1
func main() {

	// tugas ke 2 soal no 1
	fmt.Println("Hello World")

	// tugas ke 2 soal no 2
	halo := "Halo Golang"
	fmt.Println(halo)

	// tugas ke 2 soal no 3
	var kataPertama = "saya"
	var kataKedua = "senang"
	var kataKetiga = "belajaR"
	var kataKeempat = "GOLANG"
	var setence = kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempat
	fmt.Println(setence)

	// tugas ke 2 soal no 4

	var angkaPertama string = "8"
	num1, _ := strconv.ParseInt(angkaPertama, 10, 64)
	//fmt.Println(angka1)
	var angkaKedua = "5"
	num2, _ := strconv.ParseInt(angkaKedua, 10, 64)
	var angkaKetiga = "6"
	num3, _ := strconv.ParseInt(angkaKetiga, 10, 64)
	var angkaKeempat = "7"
	num4, _ := strconv.ParseInt(angkaKeempat, 10, 64)

	fmt.Println(num1 + num2 + num3 + num4)

	// tugas ke 2 soal no 5

	kalimat := "halo halo bandung"
	angka := 2021
	change := strconv.Itoa(angka)

	var Angka_gabungan = kalimat + "-" + change
	fmt.Println(Angka_gabungan)
}
